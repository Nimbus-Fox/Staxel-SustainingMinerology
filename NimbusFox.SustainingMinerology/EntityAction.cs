﻿using Staxel.EntityActions;
using Staxel.Logic;

namespace NimbusFox.SustainingMinerology {
    public class EntityAction : IEntityAction {
        public void Dispose() {
        }

        public void Load() {
        }

        public string Kind() {
            return "";
        }

        public void Start(Entity entity, EntityUniverseFacade facade) {

        }

        public void Update(Entity entity, EntityUniverseFacade facade) {

        }

        public bool Cancellable(Entity entity) {
            return false;
        }

        public bool AllowInventoryUse(Entity entity) {
            return false;
        }

        public bool PrevNextInUse(Entity entity) {
            return false;
        }

        public void Cancel(Entity entity) {
            
        }

        public bool LocksPlayerPosition() {
            return false;
        }
    }
}
